# ChatNode

ChatNode is a chatroom software in [Node.js](http://nodejs.org). It will have:

* A module system
* Online configuration panel
* Socket.IO-based client-to-server communication

Currently, it's a work in progress, but the goal is ***to not need to install anything other then Node.js to use this***.