/* 
	ChatNode
	AKA Chat-R v5.0
	(c)2015 ShadowTech Development
	Licensed under MIT
*/
// This is the main server. This loads the Module loader and configs

//First, initialization. We need important modules like sqlite3 and bcrypt for persistant storage,
//and socket.io for communication between the server and client

console.log("ChatNode v1.0 (Chat-R v5.0)")
console.log("Loading modules...")

io       = require("socket.io");    //This is our chatroom backbone, we need it for communication
path     = require("path");         //Lets us access path.join for simplicity
db       = require("binarydb");     //Our storage framework
express  = require("express");      //This hosts the chatroom client
httpstat = require("http-status");  //This allows us to send the HTTP status message name with a Jade template
jade     = require("jade");         //Allows data server side to be used client side
morgan   = require("morgan");       //This is our Express logger for logging requests to the server
fs       = require("fs");           //Allows us to open the main configuration file (config.json)
uuid     = require("node-uuid").v4; //Gives us the generation needed for auto login and online verification
bcrypt   = require("bcrypt");       //Used for password verification
https    = require("https");        //In case the user sets secure to true in config.json, it will allow us to make the HTTPS server
modSys   = require("./modsys");     //This loads the module system (module loader, events, etc)


console.log("Done.")
console.log("Loading config and data...")
//Now to first open the config (if it exists) and parse it, then load the database. If the database folder doesn't exist,
//it will create the folder and initialize the database

exists = fs.existsSync(path.join(__dirname, "config.json")); //Check to see if the file exists
if(exists){
	mainConfig = JSON.parse(fs.readFileSync(path.join(__dirname, "config.json"))); //If it exists, load config.json and parse it
} else {
	mainConfig = {
		"server": {
			"port": 8080,
			"secure": {
				"secure": false,
				"cert": "cert.crt",
				"key": "key.pem",
				"pass": null
			}
		},
		"files": {
			"database": "data/data.db",
			"web": {
				"views": "views",
				"public": "server"
			}
		}
	};
	fs.writeFileSync(path.join(__dirname, "config.json"), JSON.stringify(mainConfig, null, "\t"))
};
if(!fs.existsSync(path.join(__dirname, "data"))){
	fs.mkdirSync(path.join(__dirname, "data"));
};
try {
	data  = db.loadDb(mainConfig.files.database);
} catch(e) {
	data = {
		"firstSetup": true,
		"firstStep" : 1,
		"users": {},
		"config": {
			"general": {},
			"messages": {}
		},
		"banned": {},
		"version": 1
	}
	console.log("No database found, loading with defaults");
	db.saveDb(mainConfig.files.database, data);
}
if(data.version !== 1){
	console.error("This database isn't version 1, it's version " + '"' + data.version + '"' + ". Quitting...")
	process.exit(1)
}
//Let's grab all this important data
users  = data.users;
config = data.config;
banned = data.banlist;
online = {};
userlist = {
	"names": [],
	"style": []
}
console.log("Done.")
console.log("Starting server...")
//Now to make the Express app
app = express();
app.set("views", mainConfig.files.web.views);
app.set("view engine", "jade");
app.use(morgan("dev"));
app.use(express.static(mainConfig.files.web.public));
templateRouter = express.Router();
templateRouter.get("/", function(req, res, next){
	if(config.firstSetup){
		res.redirect("/setup");
	};
});
templateRouter.get("/setup", function(req, res, next){
	if(config.firstSetup){
		res.render("setup", {
			"date": new Date(),
			"step": data["firstStep"],
			"first": true
		});
	} else {
		res.render("setup", {
			"date": new Date(),
			"first": false,
			"title": config["general"]["title"] || "ChatNode",
			"page": req.query.page || "dashboard"
		});
	};
});
app.use(templateRouter);
app.use(function(req, res, next){
	var error = new Error("Not Found");
	error.status = 404;
	next(error);
})
app.use(function(err, req, res, next){
	var error = err.status || (err.toString().indexOf("Failed to lookup view ") !== -1 ? 404 : 500);
	
	var msg = httpstat[error]
	if(res.headersSent){
		var err = new Error(msg)
		err.status = error;
		next(err);
	}
	res.status(error).render("error", {
		"error": error,
		"message": msg.toString(),
		"title": data.firstSetup ? "ChatNode Setup" : (config["general"]["title"] || "ChatNode"),
		"date": new Date()
	});
	res.end();
})
htServ = require("http").createServer(app)
socketServ = io(htServ);
htServ.listen(mainConfig.server.port)
if (mainConfig.server.secure.secure){
	secureApp = https.createServer(mainConfig.server.secure, app)
	secureApp.listen(443)
};

//Now for actual handling of requests and such
socketServ.on("connection", function(connection){
	//We have to do a quick check on this connection to check if this IP is banned
	connection.ip = connection.request.connection.remoteAddress;
	if(banned[connection.ip] !== undefined){
		if(banned[connection.ip].current === true){
			if(banned[connection.ip].endTime <= (new Date()).getTime()){
				banned[connection.ip].current = false;
				banned[connection.ip].endTime = 0;
			} else {
				connection.emit("error", {
					"message": config["messages"]["banned"] || "This IP has been banned. Please wait until " + new Date(banned[connection.ip].endTime) + "."
				})
			}
		}
	}
	//First, initialize our important stuff, like username and ID
	connection.id      = null;
	connection.user    = null;
	connection.logIn   = false;
	//Now to emit the "connection" event, with the connection and date as the parameters
	modSys.event("connection", [connection, new Date()]);
	/*connection.on("setup", function(data){
		if(data.type === "first"){
			if(window.data.firstSetup){
				if(data.step === 1){
					config["general"]["title"] = data.title || "ChatNode";
					config["general"]["description"] = data.description || "A chatroom";
					config["general"]["owner"] = data.username;
					users[data.username] = {
						"hash": bcrypt.hashSync(data.password),
						"privLevel": 5
					};
					window.data = {
						firstSetup: true,
						firstStep: 2,
						users: window.users,
						config: window.config,
						banned: {},
						version: window.data.version
					};
					db.saveDb(mainConfig.files.database, window.data);
					connection.emit("success", {now: 2})
				}
			} else {
				connection.emit("configerror", {
					"reason": "alreadyDone",
					"message": config["messages"]["alreadyDoneFirstSetup"] || "Whoops, first setup was already done!"
				})
			}
		}
	})*/
	connection.on("login", function(username, hash){
		connection.user = user[username]; //Gives us a quick way to access that user data
		result = modSys.event("login", [connection, hash, new Date()]); //Emits the "login" event (for special login modules)
		
		if(connection.user !== undefined){
			if(!connection.user["banned"]){
				bcrypt.compare(hash, connection.user["hash"], function(err, res){
					if(res || result.passLogin){
						//Now we know at this point that this user is okay (at least, if we aren't using modules)
						connection.logIn = true;
						//The ID is used for internal purposes
						connection.id = uuid();
						//This is for ban tracking
						connection.user["lastIP"] = connection.ip;
						connection.emit("authenticated", {
							"id": id,
							"date": new Date(),
							"message": config["messages"]["authenticated"] || "You have been authenticated as " + username + "."
						});
						socketServ.emit("message", {
							"message": (config["messages"]["join"] || "%username% has joined the chat").replace(/%username%/g, connection.user.name),
							"date": new Date()
						});
						//Now, to run the "authenticated" event (in case there's something like an announcement module that works on login)
						modSys.event("authenticated", [connection, result, new Date()]);
						//Now we're done! We give control back to the server!
					} else {
						//Passwords didn't match
						res = modSys.event("incorrectPassword", [connection, result, hash, err, new Date()]);
						if(res.kill){
							connection.close(res.code || 0)
						}
						connection.emit("error", {
							"type": "incorrectPassword",
							"message": config["messages"]["incorrectPassword"] || "Incorrect password. Please try again."
						});
					};
				});
			}
		}
	})
})
console.log("Listening on port " + mainConfig.server.port + ".")


























